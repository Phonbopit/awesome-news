const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const rateLimit = require('express-rate-limit');

const app = express();

require('./db');

const port = process.env.PORT || 3001;
const isProduction = process.env.NODE_ENV === 'production';

const limiter = rateLimit({
  windowMs: 15 * 60 * 1000, // 15 minutes
  max: 100, // limit each IP to 100 requests per windowMs
  message: 'Too many request from this IP, please try again after an hour'
});

app.use(cors());
app.use(helmet());
app.use(bodyParser.json());
app.use(limiter);

app.use(require('./routes'));
/// catch 404 and forward to error handler
app.use((req, res, next) => {
  const error = new Error('Not Found');
  error.status = 404;
  next(error);
});

// development error handler
// will print stacktrace
if (!isProduction) {
  app.use((error, req, res, next) => {
    console.log(error.stack);

    res.status(error.status || 500);

    res.json({
      errors: {
        message: error.message,
        error
      }
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use((error, req, res, next) => {
  res.status(error.status || 500);

  res.json({
    erros: {
      message: error.message,
      error: {}
    }
  });
});

app.listen(port, () => {
  console.log(`Application is listening on port ${port}`);
});
