let router = require('express').Router();

const postRouter = require('../modules/posts/posts.route');
const authRouter = require('../modules/auth/auth.route');

router.use('/auth', authRouter);
router.use('/posts', postRouter);

module.exports = router;
