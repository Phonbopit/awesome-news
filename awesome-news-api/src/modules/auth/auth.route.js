let router = require('express').Router();

const { handleLogin, handleRegister, handleMe } = require('./auth.controller');
const auth = require('../../routes/auth');

router.post('/login', handleLogin);
router.post('/register', handleRegister);
router.get('/me', auth.required, handleMe);

module.exports = router;
