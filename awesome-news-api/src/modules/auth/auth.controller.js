const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../../models/User');

const handleLogin = async (req, res) => {
  const { email, password } = req.body;

  const user = await User.findOne({
    where: {
      email
    }
  });

  if (!user) {
    return res.status(401).json({
      error: 'USER_NOT_FOUND',
      message: 'Email address or password incorrect'
    });
  }

  const isPasswordMatch = await bcrypt.compare(password, user.password);

  if (!isPasswordMatch) {
    return res.status(401).json({
      error: 'INCORRECT_PASSWORD',
      message: 'Email address or password incorrect' // show user for the same message as email not found for some security.
    });
  }

  // TODO : use system variable for secret key
  const token = jwt.sign({ id: user.uuid }, 'super_secretttt', {
    expiresIn: '4h'
  });

  return res.json({
    user: {
      id: user.id,
      email: user.email
    },
    token
  });
};

const handleRegister = (req, res) => {
  const { email, password } = req.body;

  bcrypt.hash(password, 10, async (err, hash) => {
    if (!err) {
      const user = new User({
        email,
        password: hash
      });

      try {
        await user.save();
        const token = jwt.sign({ id: user.uuid }, 'super_secretttt', {
          expiresIn: '4h'
        });
        return res.json({
          user: {
            id: user.id,
            email: user.email
          },
          token
        });
      } catch (error) {
        return res.status(404).json({
          message: 'Something went wrong'
        });
      }
    }
  });
};

const handleMe = async (req, res) => {
  const { id } = req.payload;

  const user = await User.findByPK(id);

  if (user) {
    const token = jwt.sign(user.uuid, 'super_secretttt', { expiresIn: '4h' });
    return res.json({
      user: {
        id: user.id,
        email: user.email
      },
      token
    });
  }
};

module.exports = {
  handleLogin,
  handleRegister,
  handleMe
};
