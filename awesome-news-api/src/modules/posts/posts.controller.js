const Post = require('../../models/Post');

const findById = async (req, res) => {
  const { id } = req.params;

  const post = await Post.findOne({
    where: {
      id
    }
  });
  return res.json(post);
};

const findAll = async (req, res) => {
  const { limit = 20, sort = '-publishedDate' } = req.query;

  let sortKeyword;
  let direction;
  if (sort.startsWith('-')) {
    sortKeyword = sort.substring(1);
    direction = 'DESC';
  } else {
    sortKeyword = sort;
    direction = 'ASC';
  }

  const posts = await Post.findAll({
    order: [[sortKeyword, direction]],
    limit
  });

  return res.json(posts);
};

const createPost = (req, res) => {
  const payload = req.body;
  try {
    const post = new Post(payload);
    post.save();
    return res.status(201).end();
  } catch (error) {
    return res.status(401).json({
      message: 'Missing some variables'
    });
  }
};

const updatePost = async (req, res) => {
  const { id } = req.params;
  const payload = req.body;

  try {
    await Post.update(payload, {
      where: {
        id
      }
    });

    return res.json(payload);
  } catch (error) {
    return res.status(401).json({
      message: 'Missing some variables'
    });
  }
};

const deletePost = async (req, res) => {
  const { id } = req.params;

  await Post.destroy({
    where: {
      id
    }
  });

  return res.status(204).end();
};

module.exports = {
  findById,
  findAll,
  createPost,
  updatePost,
  deletePost
};
