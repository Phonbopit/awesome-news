let router = require('express').Router();

const {
  findAll,
  findById,
  createPost,
  updatePost,
  deletePost
} = require('./posts.controller');
const auth = require('../../routes/auth');

router
  .route('/')
  .get(findAll)
  .post(auth.required, createPost);

router
  .route('/:id')
  .get(findById)
  .put(auth.required, updatePost)
  .delete(auth.required, deletePost);

module.exports = router;
