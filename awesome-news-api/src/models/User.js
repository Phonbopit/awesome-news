const Sequelize = require('sequelize');
const sequelize = require('../db');

const Model = Sequelize.Model;

class User extends Model {}

User.init(
  {
    uuid: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4
    },
    email: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true
    },
    password: {
      type: Sequelize.STRING,
      allowNull: false
    }
  },
  {
    sequelize,
    modelName: 'user'
  }
);

module.exports = User;
