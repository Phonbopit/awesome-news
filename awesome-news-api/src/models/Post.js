const Sequelize = require('sequelize');
const sequelize = require('../db');

const Model = Sequelize.Model;

class Post extends Model {}

Post.init(
  {
    title: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true
    },
    description: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    coverImage: {
      type: Sequelize.STRING
    },
    publishedDate: {
      type: Sequelize.DATE,
      default: new Date()
    }
  },
  {
    sequelize,
    modelName: 'post'
  }
);

module.exports = Post;
