const Sequelize = require('sequelize');

// use .env file instead
const sequelize = new Sequelize('awesome-news-db', 'root', 'awesome', {
  host: 'awesome-news_db_1', // map with docker container host name
  dialect: 'mysql'
});

const initialSequelize = async () => {
  try {
    await sequelize.sync();
    console.log('Connection has been established successfully.');
  } catch (err) {
    console.error('Unable to connect to the database:', err);
  }
};

initialSequelize();

module.exports = sequelize;
