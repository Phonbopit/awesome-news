import React from 'react';
import { Router } from '@reach/router';
import Loadable from '@loadable/component';
import PrivateRoute from '../components/PrivateRoute';
import AppNavbar from 'components/AppNavbar';

const Posts = Loadable(() => import('../pages/posts'));
const Post = Loadable(() => import('../pages/posts/detail'));

const AdminPosts = Loadable(() => import('../pages/admin/posts'));
const AdminNewPost = Loadable(() => import('../pages/admin/new'));
const AdminEditPost = Loadable(() => import('../pages/admin/edit'));

const Login = Loadable(() => import('../pages/auth/login'));
const Register = Loadable(() => import('../pages/auth/register'));

const NotFound = () => <p>Page Not Found</p>;

export default () => (
  <>
    <AppNavbar />
    <Router>
      <Posts path="/" />
      <Post path="/posts/:id" />
      <Login path="/login" />
      <Register path="/register" />

      <PrivateRoute path="/admin/posts" component={AdminPosts} />
      <PrivateRoute path="/admin/posts/new" component={AdminNewPost} />
      <PrivateRoute path="/admin/posts/:id/edit" component={AdminEditPost} />
      <NotFound default />
    </Router>
  </>
);
