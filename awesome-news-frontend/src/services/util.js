import axios from 'axios';

const config = {
  headers: {
    'Content-Type': 'application/json'
  }
};

export function httpGet(url, args = { isAuth: false }) {
  let options = setHeaders(args.isAuth);
  options.params = args.params;
  return axios.get(url, options);
}

export function httpPost(url, data, isAuth = true) {
  const options = setHeaders(isAuth);
  return axios.post(url, data, options);
}

export function httpPut(url, data, isAuth = true) {
  const options = setHeaders(isAuth);
  return axios.put(url, data, options);
}

export function httpDelete(url, isAuth = true) {
  const options = setHeaders(isAuth);
  return axios.delete(url, options);
}

export function getTokenFromStorage() {
  return localStorage.getItem('token') || sessionStorage.getItem('token');
}

function setHeaders(isAuth) {
  const token = getTokenFromStorage();

  if (isAuth || token) {
    return {
      headers: {
        ...config.headers,
        Authorization: `Bearer ${token}`
      }
    };
  }

  return {
    ...config.headers
  };
}
