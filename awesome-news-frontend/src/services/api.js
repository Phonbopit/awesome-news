import { httpGet, httpPost, httpPut, httpDelete } from './util';

const BASE_URL =
  process.env.REACT_APP_API_BASE_URL || 'http://localhost:3001/api';

const ENDPOINT = {
  LOGIN: `${BASE_URL}/auth/login`,
  REGISTER: `${BASE_URL}/auth/register`,
  ME: `${BASE_URL}/me`,
  POSTS: {
    FIND_ALL: `${BASE_URL}/posts`,
    FIND_BY_ID: `${BASE_URL}/posts/:id`
  }
};

const login = data => httpPost(ENDPOINT.LOGIN, data);

const register = data => httpPost(ENDPOINT.REGISTER, data);

const fetchCurrentUser = () => httpGet(ENDPOINT.ME);

const fetchPosts = ({ sort = '-publishedDate' }) => {
  return httpGet(ENDPOINT.POSTS.FIND_ALL, {
    params: {
      sort
    }
  });
};

const editPost = (id, data) => {
  const url = ENDPOINT.POSTS.FIND_BY_ID.replace(':id', id);
  return httpPut(url, data);
};

const fetchPost = id => {
  const url = ENDPOINT.POSTS.FIND_BY_ID.replace(':id', id);
  return httpGet(url);
};

const deletePost = id => {
  const url = ENDPOINT.POSTS.FIND_BY_ID.replace(':id', id);
  return httpDelete(url);
};

const createPost = data => httpPost(ENDPOINT.POSTS.FIND_ALL, data);

export default {
  login,
  register,
  fetchCurrentUser,
  fetchPosts,
  editPost,
  fetchPost,
  createPost,
  deletePost
};
