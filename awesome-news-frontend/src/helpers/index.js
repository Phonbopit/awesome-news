import dayjs from 'dayjs';

export const displayDate = date => dayjs(date).format('DD MMM YYYY');
