import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import Router from './routes';

import { fetchCurrentUser } from 'redux/actions';

import './App.scss';

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchCurrentUser());
  }, [dispatch]);

  return (
    <div className="AwesomeApp">
      <Router />
    </div>
  );
}

export default App;
