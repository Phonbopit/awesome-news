import React from 'react';
import { Field, Form as FormikForm } from 'formik';
import { css } from 'emotion';
import { Link } from '@reach/router';

const AuthForm = props => {
  const { handleSubmit, error, isSubmitting, isLoginPage = false } = props;

  return (
    <div className="container">
      <div className="columns">
        <FormikForm
          className={css`
            width: 550px;
            margin: 4em auto;
            padding: 4em;
            border: 1px solid #ddd;

            @media (max-width: 768px) {
              width: 400px;
            }
          `}
          onSubmit={handleSubmit}
        >
          <h2 className="title has-text-centered">Login/Register</h2>

          <div className="field">
            <label className="label" htmlFor="email">
              Email
            </label>
            <div className="control">
              <Field
                id="email"
                name="email"
                className="input"
                type="text"
                placeholder="e.g. test@example.com"
              />
            </div>
          </div>

          <div className="field">
            <label className="label" htmlFor="password">
              Password
            </label>
            <div className="control">
              <Field
                id="password"
                name="password"
                className="input"
                type="password"
                placeholder="Password"
              />
            </div>
          </div>

          <div className="field">
            <div className="control">
              <button
                type="submit"
                className="button is-primary"
                disabled={isSubmitting}
              >
                {isLoginPage ? 'Login' : 'Register'}
              </button>
            </div>
          </div>

          {isLoginPage ? (
            <div className="field">
              <div className="control">
                <Link className="is-text" to="/register">
                  Don't have an account?
                </Link>
              </div>
            </div>
          ) : (
            <div className="field">
              <div className="control">
                <Link className="is-text" to="/login">
                  Back to login
                </Link>
              </div>
            </div>
          )}

          {error ? (
            <p
              className={css`
                color: red;
              `}
            >
              error.message
            </p>
          ) : null}
        </FormikForm>
      </div>
    </div>
  );
};

export default AuthForm;
