import React, { useState } from 'react';
import { Location, Link } from '@reach/router';

import { useSelector } from 'react-redux';

const getCurrentUserFromLocalStorage = () => {
  const currentUser = localStorage.getItem('currentUser');
  return currentUser && JSON.parse(currentUser);
};

const AppNavbar = () => {
  const [isActive, setIsActive] = useState(false);

  const currentUser =
    useSelector(state => state.auth.currentUser) ||
    getCurrentUserFromLocalStorage();

  const handleLogout = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('currentUser');

    window.location.reload();

    // TODO:
    // send logout request to server for revoke token, expires token.
  };

  const toggleMenu = () => {
    setIsActive(!isActive);
  };

  return (
    <Location>
      {({ location }) => {
        if (location.pathname === '/login') {
          return null;
        }

        return (
          <nav
            className="navbar is-light"
            role="navigation"
            aria-label="main navigation"
          >
            <div className="container">
              <div className="navbar-brand">
                <Link to="/" className="navbar-item">
                  AW-NEWS
                </Link>

                <div
                  role="button"
                  className="navbar-burger"
                  aria-label="menu"
                  aria-expanded="false"
                  onClick={toggleMenu}
                >
                  <span aria-hidden="true"></span>
                  <span aria-hidden="true"></span>
                  <span aria-hidden="true"></span>
                </div>
              </div>
              <div
                id="navbarBasic"
                className={`navbar-menu ${isActive && 'is-active'}`}
              >
                {currentUser ? (
                  <>
                    <div className="navbar-start">
                      <Link to="/admin/posts" className="navbar-item">
                        Posts
                      </Link>
                    </div>

                    <div className="navbar-end">
                      <div className="navbar-item">
                        <div className="buttons">
                          <span className="button is-text">
                            <strong>{currentUser && currentUser.email}</strong>
                          </span>
                          <button
                            onClick={handleLogout}
                            className="button is-light"
                          >
                            Log out
                          </button>
                        </div>
                      </div>
                    </div>
                  </>
                ) : (
                  <div className="navbar-end">
                    <div className="navbar-item">
                      <div className="buttons">
                        <Link to="/login" className="button is-light">
                          Log in
                        </Link>
                      </div>
                    </div>
                  </div>
                )}
              </div>
            </div>
          </nav>
        );
      }}
    </Location>
  );
};

export default AppNavbar;
