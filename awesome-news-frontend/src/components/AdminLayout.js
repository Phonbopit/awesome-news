import React from 'react';
import AppNavbar from './AppNavbar';

const AdminLayout = ({ children }) => {
  return (
    <>
      <AppNavbar />
      {children}
    </>
  );
};

export default AdminLayout;
