import React from 'react';
import { Link } from '@reach/router';

import { cx, css } from 'emotion';
import { displayDate } from 'helpers';

const PostCard = ({ post }) => {
  return (
    <div className="card">
      <div className="card-image">
        <figure className="image is-4by3">
          <img src={post.coverImage} alt={post.title} />
        </figure>
      </div>
      <div
        className={cx(
          'card-content',
          css`
            text-overflow: ellipsis;
            overflow: hidden;
            height: 240px;
          `
        )}
      >
        <div className="content">
          <h3>{post.title}</h3>
          <p>{displayDate(post.publishedDate)}</p>

          <p>{post.description}</p>
        </div>
      </div>
      <div className="card-footer">
        <Link className="card-footer-item" to={`/posts/${post.id}`}>
          Read more
        </Link>
      </div>
    </div>
  );
};

export default PostCard;
