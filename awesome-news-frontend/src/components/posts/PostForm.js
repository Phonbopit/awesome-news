import React from 'react';
import { useFormik } from 'formik';
import { Link } from '@reach/router';

const PostForm = ({ handleSubmit, post }) => {
  const defaultValue = post || {
    title: '',
    description: '',
    coverImage: '',
    publishedDate: new Date().toLocaleString()
  };

  const formik = useFormik({
    initialValues: defaultValue,
    onSubmit: values => {
      handleSubmit(values);
    }
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <div className="field">
        <label className="label" htmlFor="title">
          Title
        </label>
        <div className="control">
          <input
            id="title"
            name="title"
            type="text"
            className="input"
            onChange={formik.handleChange}
            value={formik.values.title}
          />
        </div>
      </div>

      <div className="field">
        <label className="label" htmlFor="description">
          Description
        </label>
        <div className="control">
          <textarea
            id="description"
            name="description"
            type="textarea"
            className="textarea"
            rows="5"
            onChange={formik.handleChange}
            value={formik.values.description}
          />
        </div>
      </div>

      <div className="field">
        <label className="label" htmlFor="coverImage">
          Cover Image (Urls)
        </label>
        <div className="control">
          <input
            id="coverImage"
            name="coverImage"
            type="text"
            className="input"
            onChange={formik.handleChange}
            value={formik.values.coverImage}
          />
        </div>
      </div>

      <div className="field">
        <label className="label" htmlFor="publishedDate">
          Published Date
        </label>
        <div className="control">
          <input
            id="publishedDate"
            name="publishedDate"
            type="date"
            className="input"
            onChange={formik.handleChange}
            value={formik.values.publishedDate.substring(0, 10)}
          />
        </div>
      </div>

      <div className="field">
        <div className="control">
          <button className="button is-primary" type="submit">
            Submit
          </button>
          <Link to="/admin/posts" className="button is-text">
            Cancel
          </Link>
        </div>
      </div>
    </form>
  );
};

export default PostForm;
