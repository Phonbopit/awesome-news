import React, { useEffect } from 'react';
import { navigate } from '@reach/router';
import { useSelector } from 'react-redux';

const PrivateRoute = props => {
  const { location, component: Component, children } = props;

  const isAuthenticated = useSelector(
    state =>
      state.auth.isAuthenticated || localStorage.getItem('currentUser') !== null
  );

  useEffect(() => {
    const noOnLoginPage = location.pathname !== '/login';

    if (!isAuthenticated && noOnLoginPage) {
      navigate('/login');
      return undefined;
    }
  }, [isAuthenticated, location.pathname]);

  return <Component {...props}>{children}</Component>;
};

export default PrivateRoute;
