import React from 'react';
import { withFormik } from 'formik';
import { connect } from 'react-redux';

import { login } from 'redux/actions';
import Form from 'components/auth/AuthForm';

const LoginForm = props => (
  <Form error={props.error} {...props} isLoginPage={true} />
);

const LoginWithFormik = withFormik({
  mapPropsToValues: () => ({ email: '', password: '' }),
  handleSubmit: (values, { setSubmitting, props }) => {
    const { dispatch } = props;

    dispatch(login(values));
    setSubmitting(false);
  },
  displayName: 'LoginForm'
})(LoginForm);

const mapStateToProps = state => ({
  error: state.auth.error
});

export default connect(mapStateToProps)(LoginWithFormik);
