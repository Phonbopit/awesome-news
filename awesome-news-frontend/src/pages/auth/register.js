import React from 'react';
import { withFormik } from 'formik';
import { connect } from 'react-redux';

import { register } from 'redux/actions';
import Form from 'components/auth/AuthForm';

const RegisterForm = props => <Form error={props.error} {...props} />;

const RegisterWithFormik = withFormik({
  mapPropsToValues: () => ({ email: '', password: '' }),
  handleSubmit: (values, { setSubmitting, props }) => {
    const { dispatch } = props;

    dispatch(register(values));
    setSubmitting(false);
  },
  displayName: 'RegisterForm'
})(RegisterForm);

const mapStateToProps = state => ({
  error: state.auth.error
});

export default connect(mapStateToProps)(RegisterWithFormik);
