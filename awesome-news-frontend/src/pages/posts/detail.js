import React, { useEffect } from 'react';
import { Link } from '@reach/router';

import { useDispatch, useSelector } from 'react-redux';

import { fetchPost } from 'redux/actions';

import { displayDate } from '../../helpers';

const PostDetail = ({ id }) => {
  const dispatch = useDispatch();
  const { isFetching, post } = useSelector(state => ({
    post: state.posts.detail.data,
    isFetching: state.posts.detail.isFetching
  }));

  useEffect(() => {
    dispatch(fetchPost(id));
  }, [id, dispatch]);

  if (isFetching) {
    return <p>Loading...</p>;
  }

  return (
    <>
      <section className="hero is-primary">
        <div className="hero-body">
          <div className="container">
            <h1 className="title">Awesome News</h1>
            <h2 className="subtitle">Read the latest news around the world.</h2>
          </div>
        </div>
      </section>

      <div className="container my-2">
        <div className="columns">
          <div className="column">
            <Link className="button is-link" to="/">
              Back
            </Link>
          </div>
        </div>
        <div className="columns">
          <div className="column is-8 is-offset-2">
            <div>
              <img src={post.coverImage} alt={post.title} />
              <h3 className="title">{post.title}</h3>
              <p>Published At: {displayDate(post.publishedDate)}</p>

              <br />

              <div className="content">
                <p>{post.description}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default PostDetail;
