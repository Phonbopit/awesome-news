import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { fetchPosts } from 'redux/actions';
import PostCard from 'components/posts/PostCard';

import { css, cx } from 'emotion';

const Posts = () => {
  const [filterValue, setFilterValue] = useState('-publishedDate');

  const dispatch = useDispatch();
  const { isFetching, posts } = useSelector(state => ({
    posts: state.posts.list.data,
    isFetching: state.posts.list.isFetching
  }));

  useEffect(() => {
    dispatch(fetchPosts({ sort: filterValue }));
  }, [dispatch, filterValue]);

  const handleFilterChange = e => {
    e.stopPropagation();
    setFilterValue(e.target.value);
  };

  if (isFetching) {
    return <p>Loading...</p>;
  }

  return (
    <div>
      <section className="hero is-primary">
        <div className="hero-body">
          <div className="container">
            <h1 className="title">Awesome News</h1>
            <h2 className="subtitle">Read the latest news around the world.</h2>
          </div>
        </div>
      </section>

      <div
        className={cx(
          'container',
          css`
            margin-top: 4em;

            @media (max-width: 768px) {
              padding: 1em;
            }
          `
        )}
      >
        <div className="columns">
          <div
            className={cx(
              'column',
              css`
                display: flex;
                align-items: center;
              `
            )}
          >
            <span
              className={css`
                margin-right: 8px;
              `}
            >
              Sort By
            </span>
            <div className="select">
              <select value={filterValue} onChange={handleFilterChange}>
                <option value="-publishedDate">Newest</option>
                <option value="publishedDate">Oldest</option>
              </select>
            </div>
          </div>
        </div>
        <div className="columns is-multiline">
          {posts.map(post => {
            return (
              <div className="column is-4" key={post.id}>
                <PostCard post={post} />
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default Posts;
