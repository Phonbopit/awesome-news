import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from '@reach/router';

import { fetchPosts, deletePost } from 'redux/actions';
import { cx, css } from 'emotion';

import { displayDate } from '../../helpers';

const AdminPosts = () => {
  const [filterValue, setFilterValue] = useState('-publishedDate');
  const dispatch = useDispatch();
  const { isFetching, posts } = useSelector(state => ({
    isFetching: state.posts.list.isFetching,
    posts: state.posts.list.data
  }));

  useEffect(() => {
    dispatch(fetchPosts({ sort: filterValue }));
  }, [dispatch, filterValue]);

  const handleFilterChange = e => {
    e.stopPropagation();
    setFilterValue(e.target.value);
  };

  const handleDelete = id => {
    const isConfirm = window.confirm('Do you want to delete?');

    if (isConfirm) {
      dispatch(deletePost(id));
    }
  };

  if (isFetching) {
    return <p>Loading...</p>;
  }

  return (
    <div
      className={cx(
        'container',
        css`
          margin-top: 4em;

          @media (max-width: 768px) {
            padding: 1em;
          }
        `
      )}
    >
      <div className="columns">
        <div className="column">
          <h2 className="title">Posts</h2>

          <Link className="button is-primary" to={`/admin/posts/new`}>
            New
          </Link>
        </div>
      </div>

      <div className="columns">
        <div
          className={cx(
            'column',
            css`
              display: flex;
              align-items: center;
            `
          )}
        >
          <span
            className={css`
              margin-right: 8px;
            `}
          >
            Sort By
          </span>
          <div className="select">
            <select value={filterValue} onChange={handleFilterChange}>
              <option value="-publishedDate">Newest</option>
              <option value="publishedDate">Oldest</option>
            </select>
          </div>
        </div>
      </div>

      <div className="columns">
        <div className="column">
          {posts.map(post => {
            return (
              <div
                className={cx(
                  'box',
                  css`
                    display: flex;
                    align-items: center;
                    justify-content: space-around;

                    @media (max-width: 768px) {
                      flex-direction: column;
                    }
                  `
                )}
                key={post.id}
              >
                <img
                  className={css`
                    width: 256px;

                    @media (max-width: 768px) {
                      width: 128px;
                    }
                  `}
                  src={post.coverImage}
                  alt={post.title}
                />
                <div
                  className={css`
                    flex: 2;
                    margin-left: 1em;
                    @media (max-width: 768px) {
                      margin-left: 0;
                    }
                  `}
                >
                  <p className="title">{post.title}</p>
                  <p>PublishedAt: {displayDate(post.publishedDate)}</p>
                </div>

                <div
                  className={css`
                    display: flex;
                    flex-direction: column;

                    @media (max-width: 768px) {
                      flex-direction: row;
                    }
                  `}
                >
                  <Link
                    className={cx(
                      'button is-link',
                      css`
                        width: 100px;
                        text-align: center;
                        margin-bottom: 8px;

                        @media (max-width: 768px) {
                          margin-right: 12px;
                        }
                      `
                    )}
                    to={`/admin/posts/${post.id}/edit`}
                  >
                    Edit
                  </Link>
                  <button
                    className={cx(
                      'button is-danger',
                      css`
                        width: 100px;
                        text-align: center;
                      `
                    )}
                    onClick={() => handleDelete(post.id)}
                  >
                    Delete
                  </button>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default AdminPosts;
