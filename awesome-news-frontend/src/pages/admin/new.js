import React from 'react';

import { useDispatch } from 'react-redux';
import { css } from 'emotion';
import { createPost } from 'redux/actions';
import Form from 'components/posts/PostForm';

const NewPost = () => {
  const dispatch = useDispatch();

  const handleSubmit = values => {
    dispatch(createPost(values));
  };

  return (
    <div className="container">
      <div
        className={css`
          margin-top: 4em;

          @media (max-width: 768px) {
            padding: 1em;
          }
        `}
      >
        <p className="title">New Post</p>

        <Form handleSubmit={handleSubmit} />
      </div>
    </div>
  );
};

export default NewPost;
