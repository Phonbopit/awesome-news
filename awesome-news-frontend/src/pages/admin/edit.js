import React, { useEffect } from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { css } from 'emotion';

import { editPost, fetchPost } from 'redux/actions';
import Form from 'components/posts/PostForm';

const EditPost = ({ id }) => {
  const dispatch = useDispatch();
  const { isFetching, post } = useSelector(state => ({
    post: state.posts.detail.data,
    isFetching: state.posts.detail.isFetching
  }));

  useEffect(() => {
    dispatch(fetchPost(id));
  }, [id, dispatch]);

  const handleSubmit = values => {
    dispatch(editPost(id, values));
  };

  if (isFetching) {
    return <p>Loading...</p>;
  }

  return (
    <div className="container">
      <div
        className={css`
          margin-top: 4em;
          padding: 2em;
        `}
      >
        <p className="title">Edit Post</p>

        {post && <Form handleSubmit={handleSubmit} post={post} />}
      </div>
    </div>
  );
};

export default EditPost;
