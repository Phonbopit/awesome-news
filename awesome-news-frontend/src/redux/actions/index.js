import { navigate } from '@reach/router';

import API from 'services/api';

const createActionSet = name => ({
  PENDING: `${name}_PENDING`,
  SUCCESS: `${name}_SUCCESS`,
  FAILED: `${name}_FAILED`
});

export const USER_LOGIN = createActionSet('USER_LOGIN');
export const USER_REGISTER = createActionSet('USER_REGISTER');
export const FETCH_CURRENT_USER = createActionSet('FETCH_CURRENT_USER');

export const FETCH_POSTS = createActionSet('FETCH_POSTS');
export const FETCH_POST = createActionSet('FETCH_POST');
export const EDIT_POST = createActionSet('EDIT_POST');
export const CREATE_POST = createActionSet('CREATE_POST');
export const DELETE_POST = createActionSet('DELETE_POST');

export const login = data => async dispatch => {
  dispatch({
    type: USER_LOGIN.PENDING
  });

  try {
    const response = await API.login(data);
    dispatch({
      type: USER_LOGIN.SUCCESS,
      payload: response.data
    });

    localStorage.setItem('currentUser', JSON.stringify(response.data.user));
    localStorage.setItem('token', response.data.token);

    navigate('/admin/posts');
  } catch (error) {
    dispatch({
      type: USER_LOGIN.FAILED,
      error
    });
  }
};

export const register = data => async dispatch => {
  dispatch({
    type: USER_REGISTER.PENDING
  });

  try {
    const response = await API.register(data);
    dispatch({
      type: USER_REGISTER.SUCCESS,
      payload: response.data
    });

    localStorage.setItem('currentUser', JSON.stringify(response.data.user));
    localStorage.setItem('token', response.data.token);
    navigate('/admin/posts');
  } catch (error) {
    dispatch({
      type: USER_REGISTER.FAILED,
      error
    });
  }
};

export const fetchCurrentUser = () => async dispatch => {
  dispatch({
    type: FETCH_CURRENT_USER.PENDING
  });
};

export const fetchPosts = (
  options = { sort: '-publishedDate' }
) => async dispatch => {
  dispatch({
    type: FETCH_POSTS.PENDING
  });

  try {
    const response = await API.fetchPosts(options);
    dispatch({
      type: FETCH_POSTS.SUCCESS,
      payload: response.data
    });
  } catch (error) {
    dispatch({
      type: FETCH_POSTS.FAILED,
      error
    });
  }
};

export const fetchPost = id => async dispatch => {
  dispatch({
    type: FETCH_POST.PENDING
  });

  try {
    const response = await API.fetchPost(id);
    dispatch({
      type: FETCH_POST.SUCCESS,
      payload: response.data
    });
  } catch (error) {
    dispatch({
      type: FETCH_POST.FAILED,
      error
    });
  }
};

export const editPost = (id, data) => async dispatch => {
  dispatch({
    type: EDIT_POST.PENDING
  });

  try {
    const response = await API.editPost(id, data);
    dispatch({
      type: EDIT_POST.SUCCESS,
      payload: response.data
    });

    navigate('/admin/posts');
  } catch (error) {
    dispatch({
      type: EDIT_POST.FAILED,
      error
    });
  }
};

export const deletePost = id => async dispatch => {
  dispatch({
    type: DELETE_POST.PENDING
  });

  try {
    const response = await API.deletePost(id);
    dispatch({
      type: DELETE_POST.SUCCESS,
      payload: response.data
    });

    dispatch(fetchPosts());
  } catch (error) {
    dispatch({
      type: DELETE_POST.FAILED,
      error
    });
  }
};

export const createPost = data => async dispatch => {
  dispatch({
    type: CREATE_POST.PENDING
  });

  try {
    const response = await API.createPost(data);
    dispatch({
      type: CREATE_POST.SUCCESS,
      payload: response.data
    });

    navigate('/admin/posts');
  } catch (error) {
    dispatch({
      type: CREATE_POST.FAILED,
      error
    });
  }
};
