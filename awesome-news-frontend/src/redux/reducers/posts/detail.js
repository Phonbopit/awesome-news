import { FETCH_POST } from 'redux/actions';

export const initialState = {
  isFetching: true,
  data: null,
  error: null
};

export default (state = initialState, { type, payload, error }) => {
  switch (type) {
    case FETCH_POST.PENDING:
      return {
        ...state,
        isFetching: true,
        data: null,
        error: null
      };
    case FETCH_POST.SUCCESS:
      return {
        ...state,
        isFetching: false,
        data: payload,
        error: null
      };
    case FETCH_POST.FAILED:
      return {
        ...state,
        isFetching: false,
        error,
        data: null
      };
    default:
      return state;
  }
};
