import { FETCH_POSTS } from 'redux/actions';

export const initialState = {
  isFetching: true,
  data: [],
  error: null
};

export default (state = initialState, { type, payload, error }) => {
  switch (type) {
    case FETCH_POSTS.PENDING:
      return {
        ...state,
        isFetching: true,
        data: [],
        error: null
      };
    case FETCH_POSTS.SUCCESS:
      return {
        ...state,
        isFetching: false,
        data: payload,
        error: null
      };
    case FETCH_POSTS.FAILED:
      return {
        ...state,
        isFetching: false,
        error,
        data: []
      };
    default:
      return state;
  }
};
