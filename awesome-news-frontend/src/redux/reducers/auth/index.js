import { USER_LOGIN, USER_REGISTER } from '../../actions';

export const initialState = {
  isFetching: true,
  error: null,
  currentUser: null,
  isAuthenticated: false
};

export default (state = initialState, { type, payload, error }) => {
  switch (type) {
    case USER_LOGIN.PENDING:
    case USER_REGISTER.PENDING:
      return {
        ...state,
        isFetching: true,
        currentUser: null,
        isAuthenticated: false,
        error: null
      };
    case USER_LOGIN.SUCCESS:
    case USER_REGISTER.SUCCESS:
      return {
        ...state,
        isFetching: false,
        currentUser: payload,
        isAuthenticated: true,
        error: null
      };
    case USER_LOGIN.FAILED:
    case USER_REGISTER.FAILED:
      return {
        ...state,
        isFetching: false,
        error,
        currentUser: null,
        isAuthenticated: false
      };
    default:
      return state;
  }
};
