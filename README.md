# awesome-news

SPA for news application, a simple admin console and RESTful APIs for communication with database.

- [DEMO Webiste](http://206.189.82.143/) - (User: test@example.com / Password test1234)

## Usage

```bash
docker-compose up -d
```

## Pages

> Note: If user unautorized to access page, they will force to login page.

- http://206.189.82.143/ - Home Page
- http://206.189.82.143/posts/:id - Post detail with specific id
- http://206.189.82.143/login - Login
- http://206.189.82.143/register - Register
- http://206.189.82.143/admin/posts - Posts Management (Admin)
- http://206.189.82.143/admin/posts/new - Create new Post (Admin)
- http://206.189.82.143/admin/posts/:id/edit - Edit Post (Admin)

## Nice to have

* [ ] User role (Authorization)
* [ ] Authentication Security (cookies, refreshToken, revoke token, etc.)
* [ ] Validate fields every routes
* [ ] Handle file upload
* [ ] Caching with redis
* [ ] Support news (post) with Markdown or HTML (WYSIWYG)
* [ ] Setup Gitlab (Deploy key) to auto deploy to Digital Ocean
* [ ] Database Migration & db seeders (like Ruby on Rails db:migrate)
* [ ] Test (unit, e2e)

## Docker stacks
* APIs
* Frontend
* DB with MySQL

## Project structure

* [Frontend](/awesome-news-frontend) : contain React app for Frontend.
* [Backend API](/awesome-news-api) : contain APIs server.


## APIs

- SEE [Postman API](/docs/API.postman_collection.json)

### Auth
- POST      /auth/login

Payload

| Field | required? |
| ---   | ---  |
| email | Yes |
| password | Yes |

Response

```json
// 200 OK
{
  "error": null,
  "message": null,
  "data": {
    "user": {
      "email": "",
      "name": ""
    },
    "token": "TOKEN"
  }
}

// 400 - Bad Request
{
  "error": {
    "code": 1000, // developer code
    "message": "" // developer message
  },
  "message": "Email or password is incorrect", // show user
  "data": null
}
```

- POST      /auth/register

Same as `POST /auth/login`

Example response

```
{
  "user": {
    "id": 1,
    "email": "test@example.com"
  },
  "token": "eyJh...."
}
```

- GET       /me

this is endpoint for fetch current user data, use `token` to receive data, otherwise return `401 Unauthorized`

```
{
  headers: {
    authorization: "Bearer TOKEN"
  }
}
```

### News resource.
- GET       /posts

Example

- `/posts/sort=publishedDate` : Sort by date descending order 
- `/posts/sort=-publishedDate` : Sort by date ascending : 

- GET       /posts/:id


- POST      /posts

| Field | required? |
| ---   | ---  |
| title | Yes |
| description | Yes |
| publishedDate | Yes |
| coverImage | No |

- PUT       /posts

Payload same as `POST /posts`

- DELET     /posts/:id
